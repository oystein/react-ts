'use strict';

const Express = require('express');

const port = 8080;

console.log('Starting server!');

const app = new Express();

app.use(Express.static('public'));
app.use('/js', Express.static('dist/js'));

app.listen(port, () => { console.log('Server started on port %s', port)});
