import {Action} from 'redux';

import State from './State';
import {ActionType} from './ActionType';

/* xtslint:disable:cyclomatic-complexity */
const Reducers = (previousState: State, action: Action) => {
  const actionType = ((typeof action.type === 'string' && action.type.startsWith('@@redux/INIT')) ? ActionType.INIT : action.type) as ActionType;

  console.log('In reducer! %s (%s)', action.type, actionType);

  switch( actionType ) {
    case ActionType.INIT:
      console.log('INIT from redux');
      console.dir(previousState);
      return previousState;

    case ActionType.ENABLE:
      return new State(true);

    case ActionType.DISABLE:
      return new State(false);
  }
}

export default Reducers;
