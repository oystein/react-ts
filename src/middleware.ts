import {Middleware, Action} from 'redux';
import { ActionType } from './ActionType';


export const logger: Middleware = store => next => (action: Action<ActionType>) => {
  console.group('ActionType: %s', ActionType[action.type]);
  console.info('dispatching', action);
  let result = next(action)
  console.log('next state', store.getState())
  console.groupEnd()
  return result
}
