export enum ActionType {
  INIT,
  ENABLE,
  DISABLE
}
