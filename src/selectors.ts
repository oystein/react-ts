import State from "./State";

export const stateEnabledFlag = (state: State) => state.enabled;
