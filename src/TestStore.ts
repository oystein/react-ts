import {Store, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import State from './State';
import Reducers from './Reducers';
import {logger} from './middleware';

const state = new State(false);

const TestStore: Store<State> = createStore(
                                  Reducers, state,
                                  applyMiddleware(
                                    thunk,
                                    logger
                                  )
                                );

export default TestStore;
