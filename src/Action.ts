import {Dispatch} from 'redux';
import {ActionType} from './ActionType';

export type EnableFlag = () => (dispatch: Dispatch) => void;
export const enableFlag: EnableFlag = () => (dispatch: Dispatch) => {
  dispatch({
    type: ActionType.ENABLE
  });
}

export type DisableFlag = () => (dispatch: Dispatch) => void;
export const disableFlag: DisableFlag = () => (dispatch: Dispatch) => {
  dispatch({
    type: ActionType.DISABLE
  });
}
