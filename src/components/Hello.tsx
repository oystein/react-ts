import * as React from "react";

import {enableFlag, EnableFlag, disableFlag, DisableFlag} from '../Action';
import State from "../State";
import * as selectors from '../selectors';

import { connect } from "react-redux";

interface TagProps {
  compiler: string;
  framework: string;
}

interface StateProps {
  enabledState: boolean;
}

interface DispatchProps {
  doEnable: EnableFlag;
  doDisable: DisableFlag;
}

type Props = TagProps & StateProps & DispatchProps;
// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
class HelloComponent extends React.Component<Props, {}> {
    render() {
      console.dir(this.props);
        return (
          <div>
          <h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>
          <button onClick={this.handleEnable}>Enable</button>
          <button onClick={this.handleDisable}>Disable</button>
          <p>State flag is set to {this.props.enabledState ? 'true' : 'false'}</p>
          </div>
        )}

   private handleEnable = () => {
    this.props.doEnable();
  }

   private handleDisable = () => {
    this.props.doDisable();
  }
}

const mapStateToProps = (state: State): StateProps => {
  return ({
    enabledState: selectors.stateEnabledFlag(state),
  });
}

const mapDispatchToProps = {
  doEnable: enableFlag,
  doDisable: disableFlag,
}

export const Hello = connect<StateProps, DispatchProps, TagProps>(mapStateToProps, mapDispatchToProps)(HelloComponent);
