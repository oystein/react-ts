import * as React from "react";
import * as ReactDOM from "react-dom";

import {Provider} from 'react-redux';

import { Hello } from "./components/Hello";
import TestStore from "./TestStore";

ReactDOM.render(
  <Provider store={TestStore}>
    <Hello compiler="TypeScript" framework="React" />
  </Provider>,
  document.getElementById("example")

);
